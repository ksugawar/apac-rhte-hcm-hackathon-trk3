# apac-rhte-hcm-hackathon-trk3

APAC RHTE 2019 Hands-on Lab R2008-A: Track 3 "Extend CloudForms Capabilities via Ansible Playbooks"

 My Exntension
===============

CloudForms 4.7 has a bug that prevents it from creating a Cinder volume in any OpenStack project other than admin, and in any availability zone other than the default zone (BZ#1549128).
It's fixed in CFME 5.11 (CF 5.0) but I had to implement a work-around until 5.0 becomes GA.

This playbook can be invoked from a custom button placed in CF UI.

 Prerequisites
===============

- You have an OpenStack cloud provider registered to your CloudForms
- You have an Ansible Tower automattion provider registered to your CloudForms
- You make the playbook cinder-volume-create.yml a Job Template on the Tower
- You generate a Service Dialog from the Job Template
- You create a custom button that invokes the Job Template via the Service Dialog